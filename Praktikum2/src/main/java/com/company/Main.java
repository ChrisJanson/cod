package com.company;


import org.apache.commons.math3.analysis.*;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.lang.Math;

public class Main {

    public static void main(String[] args) throws IOException {

    aufgabe1();



    }



    public static void aufgabe1() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter q");
        int q = Integer.parseInt(br.readLine());
        int d = 0;
        while(true){
        System.out.println("Enter d");
        d = Integer.parseInt(br.readLine());

        if(d < 2 || d > q-2) {
            System.out.println("d ist Falsch gewählt ( 2 <= d <= q-2 )");
            continue;
        }
        break;
        }

        // bestimme Primitives element
        Integer[] ergebnisse = new Integer [q-1];
        int primitiveselement = 0;
        for ( int alpha = 2; alpha < 100000 ; alpha++) {

            for (int i = 0; i < q-1; i++ ){
                ergebnisse[i] = ((int)Math.pow(alpha,i))%q;
            }
            HashSet<Integer> numberSet = new HashSet<Integer>(Arrays.asList(ergebnisse));
            boolean found = (ergebnisse.length>numberSet.size());

            if (found) {
                continue;
            } else {
                if(Math.pow(alpha, q-1)%q == 1){
                    primitiveselement = alpha;
                    break;
                }
                continue;
            }
        }
        if(primitiveselement == 0){
            System.out.print("q ist keine Primzahl -> kein primitives element !");
            System.exit(-1);
        } else {
        System.out.println("Primitives element ist: " + primitiveselement); }


        // Generatorpolynom

        PolynomialFunction gx = null;
        for (int i = 1; i <= d-1; i++) {
            double[] coeff = {q - ergebnisse[i], 1}; // ergebniss + x
            final PolynomialFunction f = new PolynomialFunction(coeff);
            if(i == 1){
                gx = f;
            } else {
                gx = gx.multiply(f);
            }

        }

        // Calculate the Mod
        double[] tempmod = gx.getCoefficients();
        for(int i = 0; i < tempmod.length; i++){
            tempmod[i] = tempmod[i] % q;
        }
        gx = new PolynomialFunction(tempmod);


        System.out.println("Generatorpolynom ist: " + gx.toString());


        // Kontrollpolynom
        double[] initcoeff = {q - 1, 1}; // ergebniss + x
        PolynomialFunction hx = new PolynomialFunction(initcoeff);

        for(int i = d; i <= q-2; i++){
            double[] coeff = {q - ergebnisse[i], 1}; // ergebniss + x
            final PolynomialFunction f = new PolynomialFunction(coeff);
            hx = hx.multiply(f);
        }

        // Calculate the Mod
        tempmod = hx.getCoefficients();
        for(int i = 0; i < tempmod.length; i++){
            tempmod[i] = tempmod[i] % q;
        }
        hx = new PolynomialFunction(tempmod);


        System.out.println("Kontrollpolynom ist: " + hx.toString());


        // Generatormatrix
        double [][] generatormatrix = new double [q-d][q-1];
        double[] coeff = gx.getCoefficients();
                for(int i = 0; i < q-d; i++) {
                    for (int j = 0; j < q-2; j++){
                        if(i>0){
                            generatormatrix[i][j+i] = (int)coeff[j];
                        } else {
                        generatormatrix[i][j] = (int)coeff[j];}
                    }
                }
        System.out.print("Generatormatrix :");
        outputMatrix(generatormatrix);
        System.out.println("");

        //Kontrollmatrix

        double [][] kontrollmatrix = new double [d-1][q-1];
        double[] coeff2 = hx.getCoefficients();
        for(int i = 0; i < d-1; i++) {
            for (int j = 0; j < coeff2.length; j++){
                kontrollmatrix[i][j+i] = (int)coeff2[coeff2.length - 1  - j];
            }
        }
        System.out.print("Kontrollmatrix :");
        outputMatrix(kontrollmatrix);
        System.out.println("");

        // VanderMonde
        double [][] VanderMonde = new double [d-1][q-1];

        for(int i = 0; i < d-1; i++) {
            for (int j = 0; j < q-1; j++){
                if(j == 0){
                    VanderMonde[i][j] = 1;
                } else {
                    VanderMonde[i][j] = Math.pow(primitiveselement,((i+1)*(j)))%q;
                }
            }
        }
        System.out.print("VanderMonde-Matrix :");
        outputMatrix(VanderMonde);
        System.out.println("");



        // Beiweis von Aufgabe 1.5 ( Multipliziere Generatormatrix mit Transponierte Vandermond Matrix, Ergebniss 0,
        // dann ist dies eine Gültige Kontrollmatrix
        RealMatrix vandermondMatrix = new Array2DRowRealMatrix(VanderMonde);
        RealMatrix generatorMatrix = new Array2DRowRealMatrix(generatormatrix);



        RealMatrix TvandermondMatrix = vandermondMatrix.transpose();
        RealMatrix ergebnismatrix = generatorMatrix.multiply(TvandermondMatrix);
        System.out.println("Ergebniss der der Multiplikation GeneratorM x TransponierteVandermond Matrix");


        for (int i = 0; i < ergebnismatrix.getRowDimension(); i++) {
            tempmod = ergebnismatrix.getRow(i);
            for (int n = 0; n < tempmod.length; n++ ){
                    tempmod[n] = tempmod[n] % q;
                    System.out.print(tempmod[n]+ " ");
            }
            System.out.println();

        }


    }

    public static int [] [] generateReedMullerGMatrix (int r, int m) {
        if ( r >= 1 && r <=m ) {
            // Matrix Top Right
            int [] [] subMatrix1 = generateReedMullerGMatrix(r, m-1);
            // Matrix Top Left
            int [] [] subMatrix2 = subMatrix1;
            // Matrix Bottom Left
            int [] [] subMatrix4 = generateReedMullerGMatrix(r - 1, m-1);
            // Matrix Bottom Right
            int [] [] subMatrix3 = new int [subMatrix4.length] [subMatrix1[0].length];


            // combine all
            int [] [] Matrix = new int [subMatrix2.length + subMatrix3.length][subMatrix1[0].length * 2];

            // Add Matrix1
            for (int row = 0; row < subMatrix1.length; row++ ){
                for (int column = 0; column < subMatrix1[0].length; column++){
                    Matrix[0 + row][subMatrix2[0].length + column] = subMatrix1[row][column];
                }
            }

            // Add Matrix2
            for (int row = 0; row < subMatrix2.length; row++ ){
                for (int column = 0; column < subMatrix2[0].length; column++){
                    Matrix[0 + row][column] = subMatrix2[row][column];
                }
            }

            // Add Matrix3
            for (int row = 0; row < subMatrix3.length; row++ ){
                for (int column = 0; column < subMatrix3[0].length; column++){
                    Matrix[subMatrix2.length + row][column] = subMatrix3[row][column];
                }
            }

            // Add Matrix4
            for (int row = 0; row < subMatrix4.length; row++ ){
                for (int column = 0; column < subMatrix4[0].length; column++){
                    Matrix[subMatrix1.length  + row][subMatrix3[0].length + column] = subMatrix4[row][column];
                }
            }
            return Matrix;

        }
        else if( r == 0) {
            int [][] temp = new int [1] [(int)Math.pow(2,m)];
            for (int i = 0; i < temp.length; i++) {
                for (int n = 0; n < temp[0].length; n++) {
                    temp[i][n] = 1;
                }
            }
            return temp;
        }
        else if ( r > m) {
            return generateReedMullerGMatrix(m,m);
        }



        return null;
    }

    public static int [] getErrorPosition(int [] [] transposed, int [] message){
        int [] syndrom = new int [transposed[0].length];
        int [] error = new int [transposed.length];
        int errorcount = 0;

        for (int i = 0; i < message.length; i++) {
            if(message[i] == 1) {
                for (int l = 0; l < syndrom.length; l++) {
                    syndrom[l] = (syndrom[l] + transposed[i][l]) %2;
                }
            }
        }
        for (int i = 0; i < transposed.length; i++){
            if(Arrays.equals(syndrom, transposed[i])){
                    error[i]=1;
                    errorcount++;
            }
        }
        if(errorcount == 0) {
            System.out.println("No Error in Message");
            return null;
        }
        System.out.println("Number of Errors: " + errorcount);
        return error;
    }

    public static int [] decodeMessage(int [] message, int [] error) {

        if (error == null) {

            return message;
        }

        for(int i = 0; i < message.length; i++) {
            message[i] = (message[i] + error[i]) % 2;
        }
        return message;
    }

    public static void outputMatrix(double [][] matrix){
        for (int i = 0; i < matrix.length; i++) {
            System.out.println();
            for (int l = 0; l< matrix[0].length; l++){
                System.out.print(matrix[i][l] + " ");
            }
        }
        System.out.println();
    }

    public static int[][] createParityCheck(int m){
        int [][] paritiyCheck = new int [m][(int)Math.pow(2,m)-1];

        for (int i=0; i < paritiyCheck.length ; i++){
            int pattern = (int)Math.pow(2,m-i);
            for (int l=0 ; l< paritiyCheck[0].length+1; l++ ){
                if(l>0) {
                    int result = (l % pattern);
                    if(result < (pattern/2)){
                        paritiyCheck[i][l-1]=0;
                    } else
                        paritiyCheck[i][l-1]=1;
                }
            }
        }

        return paritiyCheck;
    }

    public static int[][] createTransposedParityCheck(int[][] paritycheck, int m){
        int [][] transposed = new int [(int)Math.pow(2,m)-1][m];
        for (int i = 0; i<paritycheck[0].length; i++){
            for(int l = 0; l<paritycheck.length; l++){
                transposed[i][l] = paritycheck[l][i];
            }
        }
        return transposed;
    }


}
